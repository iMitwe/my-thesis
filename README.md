# Des inégalités basées sur l’opérateur aux q-différences #

Ceci est ma thèse sur "Des inégalités basées sur l’opérateur aux q-différences" 

### A quoi sert ce répertoire ? ###

* Dans ce travail, nous allons travailler sur certaines des inégalités générales découlant des travaux (surtout de Hamza) cités et en tirer leur équivalence dans le calcul aux q-différences.
* Version 0.1
* [Vous pouvez apprendre Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Comment utiliser ?###

* Compilez avec **pdflatex** ou **LaTex**, le fichier *inegalites.tex*

### Contributions ###

* Pas encore

### Propriété de qui ? ###

* Repo de @iMitwe