\select@language {french}
\contentsline {chapter}{D\IeC {\'e}dicaces}{i}{chapter*.1}
\contentsline {chapter}{Remerciements}{ii}{chapter*.2}
\contentsline {chapter}{Introduction g\IeC {\'e}n\IeC {\'e}rale}{1}{chapter*.3}
\contentsline {chapter}{\numberline {1}\IeC {\'E}l\IeC {\'e}ments du calcul aux q-diff\IeC {\'e}rences }{4}{chapter.1}
\contentsline {section}{\numberline {1.1}La q-d\IeC {\'e}rivation et q-int\IeC {\'e}gration}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}La d\IeC {\'e}riv\IeC {\'e}e }{5}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}L'int\IeC {\'e}grale}{7}{subsection.1.1.2}
\contentsline {section}{\numberline {1.2}Propri\IeC {\'e}t\IeC {\'e}s fondamentales de la \\ diff\IeC {\'e}rentiation et de l\IeC {\textquoteright }int\IeC {\'e}gration aux \\ q-diff\IeC {\'e}rences}{10}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}D\IeC {\'e}riv\IeC {\'e}e du produit de deux fonctions}{10}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}D\IeC {\'e}riv\IeC {\'e}e du rapport de deux fonctions}{12}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}D\IeC {\'e}riv\IeC {\'e}e d\IeC {\textquoteright }une fonction compos\IeC {\'e}e}{13}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}D\IeC {\'e}riv\IeC {\'e}e d\IeC {\textquoteright }une fonction inverse}{13}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}Principes fondamentaux de la q-analyse }{14}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Premier principe}{14}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Deuxi\IeC {\`e}me principe}{14}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}Int\IeC {\'e}gration par parties}{16}{section.1.4}
\contentsline {section}{\numberline {1.5}La fonction q-exponentielle}{17}{section.1.5}
\contentsline {section}{\numberline {1.6}\IeC {\'E}quation aux q-diff\IeC {\'e}rences lin\IeC {\'e}aire du \\ premier ordre}{18}{section.1.6}
\contentsline {chapter}{\numberline {2}In\IeC {\'e}galit\IeC {\'e}s aux q-diff\IeC {\'e}rences}{24}{chapter.2}
\contentsline {section}{\numberline {2.1}In\IeC {\'e}galit\IeC {\'e}s de q-H\IeC {\"o}lder et de q-Cauchy-Schwarz}{27}{section.2.1}
\contentsline {section}{\numberline {2.2}In\IeC {\'e}galit\IeC {\'e} de q-Minkowski}{30}{section.2.2}
\contentsline {section}{\numberline {2.3}In\IeC {\'e}galit\IeC {\'e} de q-Gr\IeC {\"o}nwall}{36}{section.2.3}
\contentsline {section}{\numberline {2.4}In\IeC {\'e}galit\IeC {\'e} de q-Bernoulli}{38}{section.2.4}
\contentsline {section}{\numberline {2.5}In\IeC {\'e}galit\IeC {\'e} de q-Lyapunov}{39}{section.2.5}
\contentsline {chapter}{Conclusion}{44}{chapter*.11}
\contentsline {chapter}{Bibliographie}{46}{chapter*.11}
