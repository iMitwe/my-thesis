\select@language {french}
\select@language {francais}
\contentsline {chapter}{Remerciements}{2}{chapter*.2}
\contentsline {chapter}{R\IeC {\'e}sum\IeC {\'e}}{4}{chapter*.3}
\contentsline {chapter}{Introduction g\IeC {\'e}n\IeC {\'e}rale}{8}{chapter*.4}
\contentsline {chapter}{\numberline {1}Pr\IeC {\'e}liminaires }{10}{chapter.1}
\contentsline {section}{\numberline {1.1}Rappels et d\IeC {\'e}finitions}{11}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Les suites num\IeC {\'e}riques }{12}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Suites de fonctions \IeC {\`a} valeurs dans \textbf {R}}{13}{subsection.1.1.2}
\contentsline {subsubsection}{\numberline {1.1.2.1}Convergence simple dans $\mathbb {R}$}{13}{subsubsection.1.1.2.1}
\contentsline {subsubsection}{\numberline {1.1.2.2}Convergence uniforme dans $\mathbb {R}$}{14}{subsubsection.1.1.2.2}
\contentsline {subsection}{\numberline {1.1.3}S\IeC {\'e}ries de fonctions}{15}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Point fixe}{17}{section.1.2}
\contentsline {section}{\numberline {1.3}q-D\IeC {\'E}RIV\IeC {\'E}}{21}{section.1.3}
\contentsline {section}{\numberline {1.4}q-INT\IeC {\'E}GRALE}{22}{section.1.4}
\contentsline {section}{\numberline {1.5} Calcul associ\IeC {\'e} \IeC {\`a} l'op\IeC {\'e}rateur aux diff\IeC {\'e}rences g\IeC {\'e}n\IeC {\'e}ral}{22}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}L'op\IeC {\'e}rateur aux diff\IeC {\'e}rences g\IeC {\'e}n\IeC {\'e}ral}{25}{subsection.1.5.1}
\contentsline {section}{\numberline {1.6}Exponentielles}{30}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}Exponentielles}{30}{subsection.1.6.1}
\contentsline {chapter}{\numberline {2}Quelques in\IeC {\'e}galit\IeC {\'e}s pour $\beta $}{33}{chapter.2}
\contentsline {section}{Introduction}{33}{section*.6}
\contentsline {section}{\numberline {2.1}In\IeC {\'e}galit\IeC {\'e} de $\beta $-H\IeC {\"o}lder}{34}{section.2.1}
\contentsline {subsection}{L\IeC {\textquoteright }in\IeC {\'e}galit\IeC {\'e} de $\beta $-Cauchy-Schwarz}{35}{section*.7}
\contentsline {section}{\numberline {2.2}L\IeC {\textquoteright }in\IeC {\'e}galit\IeC {\'e} de $\beta $-Minkowski}{35}{section.2.2}
\contentsline {section}{\numberline {2.3}In\IeC {\'e}galit\IeC {\'e} de $\beta $-Gr\IeC {\"o}nwall}{36}{section.2.3}
\contentsline {section}{\numberline {2.4}L'in\IeC {\'e}galit\IeC {\'e} de $\beta $-Bernoulli}{38}{section.2.4}
\contentsline {section}{\numberline {2.5}L\IeC {\textquoteright }in\IeC {\'e}galit\IeC {\'e} de $\beta $-Lyapunov}{39}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}L'in\IeC {\'e}galit\IeC {\'e} de $\beta $-Lyapunov}{41}{subsection.2.5.1}
\contentsline {chapter}{\numberline {3}Application pour $\beta $ particulier}{43}{chapter.3}
\contentsline {section}{\numberline {3.1}Int\IeC {\'e}grale de Jackson}{43}{section.3.1}
\contentsline {section}{\numberline {3.2}Int\IeC {\'e}grale de Hahn}{44}{section.3.2}
\contentsline {section}{\numberline {3.3}L\IeC {\textquoteright }in\IeC {\'e}galit\IeC {\'e} de Hahn-Gronwall}{45}{section.3.3}
\contentsline {section}{\numberline {3.4}In\IeC {\'e}galit\IeC {\'e} de $\beta $-Opial}{47}{section.3.4}
\contentsline {chapter}{Conclusion}{50}{chapter*.9}
